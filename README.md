# pymink
A collection of simple Python functions for processing, sampling, and segmenting 2D/3D (grayscale, 8-bit) microstructure images.

## Requirements
- numpy
- matplotlib
- scikit-image
- scipy

## Usage
Simply import desired functions. For examples:
```python
# import a submodule, then call a function
import pymink.util
new_img = pymink.util.relabel_phase(img, 0, 1)

# import all functions in a submodule, then call a function
from pymink.segment import *
new_img = waterhsed_with_grad(img)
```

### Images as numpy ndarrays
The `img` variable mentioned above denotes a single image data. Any image data can be represented as arrays of numbers. All functions in this repository consider `img` as a numpy `ndarray`, either in 2D (`img.ndims == 2`) or 3D (`img.ndims == 3`), and the data type should be unsigned 8 bit integer (`img.dtype == np.uint8`). Therefore, before using any of the functions, please convert your image data into `ndarray`.

### Submodules
Each submodule focuses on a specific task. The submodules are explained below: 
- `display`: for visualization purposes
- `measure`: for analyzing/measuring properties such as volume fraction
- `morphology`: for image-processing morphological operations
- `sampling`: for sampling/cropping subimages from a large image data
- `segment`: for image segmentation
- `util`: for utility functions that may be used in other submodules
