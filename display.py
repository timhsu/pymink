import numpy as np
import matplotlib.pyplot as plt

from .util import check_img_is_3d

def image_batch(img_batch, fig_dim=(5,5), figsize=(10,10)):
    """
    Displays a collection of 2D images in a grid-like fashion.
    Grid dimension and size can be specified.
    """
    nrow, ncol = fig_dim
    fig = plt.figure(figsize=figsize)
    for i, img in enumerate(img_batch):
        try:
            ax = fig.add_subplot(ncol, nrow, i+1)
        except ValueError:
            break
        else:
            ax.axis('off')
            ax.imshow(img, cmap='gray')
    fig.tight_layout()

@check_img_is_3d
def slices_from_volume(img, slice_idx=None, num_slices=6, fig_dim=(6,1), figsize=(10,10)):
    """
    Displays x-y slices from a 3D image volume using the `image_batch`
    function.

    `slice_idx` determines which slices (along z-axis) to display.
    If `slice_idx` is not specified, `num_slices` is used instead, 
    which means the code will display `num_slices` of evenly 
    spaced slices.
    
    Returns `slice_idx`.
    """
    z,_,_ = img.shape
    if slice_idx:
        img_batch = img[slice_idx]
    else:
        slice_idx = range(0, z, int(z/(num_slices-1)))
        img_batch = img[slice_idx]
    image_batch(img_batch, fig_dim, figsize)
    return slice_idx