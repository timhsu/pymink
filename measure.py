import numpy as np

from .util import check_img_is_uint8

@check_img_is_uint8
def get_num_phases(img):
    return len(np.unique(img))

@check_img_is_uint8
def get_phase_fraction(img, phase_id=1):
    total_vol = np.prod(img.shape)
    phase_vol = np.sum(img == phase_id)
    return phase_vol / total_vol