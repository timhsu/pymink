import numpy as np
from skimage import morphology as morph

from .util import check_img_is_uint8

def dilate_phase(img, struct_element, phase_id):
    """
    Morphologically dilate a phase using the specified `struct_element`.

    Returns an image with the phase dilated.
    """
    img_copy = np.copy(img)
    binary = img_copy == phase_id
    dilated = morph.binary_dilation(binary, struct_element)
    img_copy[dilated] = phase_id
    return img_copy

@check_img_is_uint8
def dilate_phase_sequence(img, struct_element, phase_id_order=(1,3,2)):
    """
    Run the `dilate_phase` function multiple times, dilating the specified
    phases in sequence.

    Returns an image with the phases dilated.
    """
    img_copy = np.copy(img)
    for phase_id in phase_id_order:
        img_copy = dilate_phase(img_copy, struct_element, phase_id)
    return img_copy