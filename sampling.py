import numpy as np
from .util import check_img_is_3d, check_img_is_uint8

def get_random_corner(image_size, subimage_size):
    """
    Randomly place a smaller subimage within a larger image.

    Returns the subimage corner index.
    """
    image_size = np.array(image_size)
    subimage_size = np.array(subimage_size)
    idx_max = image_size - (subimage_size-1)
    return tuple(np.random.randint(i) for i in idx_max)

def crop_ndarray(a, x=(0,0), dx=(64,64)):
    """
    Crop a n-dimensional subimage from a n-dimensional image.
    But this function is rather slow.

    Returns the cropped image.
    """
    assert a.ndim == len(x) == len(dx)
    for x_i, dx_i, axis in zip(x, dx, range(a.ndim)):
        a = np.take(a, range(x_i, x_i+dx_i), axis=axis)
    return a

def crop_ndarray_2d(a, x=(0,0), dx=(64,64)):
    """
    Crop a 2D subimage from a 2D image.

    Returns the cropped image.
    """
    return a[x[0]:(x[0]+dx[0]),
             x[1]:(x[1]+dx[1])]

def crop_ndarray_3d(a, x=(0,0,0), dx=(64,64,64)):
    """
    Crop a 3D subimage from a 3D image.

    Returns the cropped image.
    """
    return a[x[0]:(x[0]+dx[0]),
             x[1]:(x[1]+dx[1]),
             x[2]:(x[2]+dx[2])]

def sample_3d_subimage(img, subimage_size=(64,64,64)):
    """
    Randomly sample a 3D subimage from a 3D image.

    Returns the sampled image.
    """
    idx = get_random_corner(img.shape, subimage_size)
    subimage = crop_ndarray_3d(img, idx, subimage_size)
    return subimage

def sample_2d_subimage(img, subimage_size=(64,64)):
    """
    Randomly sample a 2D subimage from a 3D image.

    Returns the sampled image.
    """
    # Only sampling images that lie on x-y plane
    Z,_,_ = img.shape
    img = img[np.random.randint(Z), ...]
    idx = get_random_corner(img.shape, subimage_size)
    subimage = crop_ndarray(img, idx, subimage_size)
    return subimage

def apply_symmetry_ops(img):
    """
    Randomly apply mirror and/or rotation symmetry operations 
    to the image.

    Returns a flipped/rotated image.
    """
    if img.ndim == 2:
        img = np.rot90(img, k=np.random.randint(4))
        if np.random.choice([True, False]): img = np.fliplr(img)
        if np.random.choice([True, False]): img = np.flipud(img)
    elif img.ndim == 3:
        img = np.rot90(img, k=np.random.randint(4),    axes=(1,2))
        img = np.rot90(img, k=np.random.choice([0,2]), axes=(0,1))
        img = np.rot90(img, k=np.random.choice([0,2]), axes=(0,2))
        if np.random.choice([True, False]): img = np.flip(img, axis=0)
        if np.random.choice([True, False]): img = np.flip(img, axis=1)
        if np.random.choice([True, False]): img = np.flip(img, axis=2)
    else:
        raise Exception("img is neither 2D nor 3D")
    return img

@check_img_is_3d
@check_img_is_uint8
def sample_subimage_batch(img,
                          subimage_size=(64,64,64),
                          batch_size=64,
                          apply_symmetry=True):
    """
    Randomly sample a batch of subimages from a 3D image.
    `subimage_size` can either 2D or 3D.
    
    Returns a list of subimages.
    """
    if len(subimage_size) == 2:
        sample_func = sample_2d_subimage
    elif len(subimage_size) == 3:
        sample_func = sample_3d_subimage
    else:
        raise Exception("subimage dimension is neither 2D nor 3D")
    subimages = [sample_func(img, subimage_size) for _ in range(batch_size)]
    if apply_symmetry:
        subimages = [apply_symmetry_ops(s) for s in subimages]
    return subimages