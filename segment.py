import numpy as np 
from skimage.morphology import watershed
from scipy import ndimage

from .util import check_img_is_uint8

def sobel_gradients(img):
    sobels = [ndimage.sobel(img, axis=i) for i in range(img.ndim)]
    grad   = np.sqrt(np.sum([s**2 for s in sobels], axis=0))
    return grad

def select_by_range(ndarray, int_range):
    int_min, int_max = int_range
    return np.logical_and(ndarray >= int_min, ndarray <= int_max)

@check_img_is_uint8
def watershed_with_grad(img,
                        grad_range=[(0,1), (0,1), (0,1)],
                        int_range=[(0,25), (75,110), (170,255)]):
    """
    Apply watershed segmentation algorithm with seeds specified by
    `grad_range` and `int_range`.

    Returns a segmented image. 
    """
    assert len(np.unique(img)) >= 10, "img might be already segmented."
    assert len(grad_range) == len(int_range)

    grad = sobel_gradients(img/255)
    
    num_phases = len(grad_range)
    markers = np.zeros(img.shape)
    for n, g, i in zip(range(num_phases), grad_range, int_range):
        markers[select_by_range(grad, g) & select_by_range(img, i)] = n+1

    labels = watershed(grad, markers)
    return labels.astype(np.uint8)