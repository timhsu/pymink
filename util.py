import numpy as np
import functools
from glob import glob

def map_glob(func, glob_str):
    return [func(fname) for fname in sorted(glob(glob_str))]

def check_img_is_3d(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        if args[0].ndim != 3:
            raise Exception("first function argument is not a 3D ndarray")
        return func(*args, **kwargs)
    return wrapper

def check_img_is_uint8(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        if args[0].dtype != np.uint8:
            raise Exception("first function argument is not a uint8 ndarray")
        return func(*args, **kwargs)
    return wrapper

@check_img_is_uint8
def relabel_phase(img, phase_from=0, phase_to=1):
    img_new = np.copy(img)
    img_new[img == phase_from] = phase_to
    return img_new